﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV1
{
    class Timenote : Note
    {
        private DateTime time;

        public Timenote() : base()
        {
            time = DateTime.Now;
        }

        public Timenote(string text, int priorityLevel, string owner)
               : base(text, priorityLevel, owner)
        {
            this.time = DateTime.Now;
        }

        public Timenote(string text, int priority, string owner, DateTime time)
               : base(text, priority, owner)
        {
            this.time = time;
        }

        public DateTime Time
        {
            get { return this.time; }
            set { this.time = value; }
        }
        public override string ToString()
        {
            return base.ToString() + "\n" + this.Time;
        }
    }

}
