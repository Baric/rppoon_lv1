﻿using System;

namespace RPPOON_LV1
{
    class Program
    {
        static void Main(string[] args)
        {
          
            Note note;
            note = new Note();
            Console.WriteLine(note.getOwner());
            Console.WriteLine("Note: " + note.getText());
            note.setText("Go shopping");
            Console.WriteLine(note.getOwner());
            Console.WriteLine("Note: " + note.getText());

            note = new Note("Clean the driveway", 1, "Matija");
            Console.WriteLine(note.getOwner());
            Console.WriteLine("Note: " + note.getText());

            note = new Note("Attend the lecture");
            Console.WriteLine(note.getOwner());
            Console.WriteLine("Note: " + note.getText());

            
            Console.Write("Import notes: ");
            note.Text = Console.ReadLine();
            Console.WriteLine();

            Console.WriteLine(note.Owner);
            Console.WriteLine("Note: " + note.Text);

            
            Console.WriteLine(note.ToString());

            
            Console.WriteLine("Timenote");
            note = new Timenote();
            Console.WriteLine(note.ToString());
        }
    }
}

