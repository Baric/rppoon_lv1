﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV1
{
    class Note
    {
        private String text;
        private int priorityLevel;
        private String owner;
        #region
        public string getText() { return this.text; }
        public int getLevel() { return this.priorityLevel; }
        public string getOwner() { return this.owner; }
        public void setText(string text) { this.text = text; }
        public void setLevel(int lvl) { this.priorityLevel = lvl; }
        private void setOwner(string owner) { this.owner = owner; }
        #endregion

        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public int PriorityLevel
        {
            get { return this.priorityLevel; }
            set { this.priorityLevel = value; }
        }
        public string Owner
        {
            get { return this.owner; }
            private set { this.owner = value; }
        }


        #region
        public Note()
        {
            text = " ";
            priorityLevel = 0;
            owner = "Matija";
        }
        public Note(string text)
        {
            this.text = text;
            this.priorityLevel = 0;
            this.owner = "Matija";
        }

        public Note(string text, int priority, string owner)
        {
            this.text = text;
            this.priorityLevel = priority;
            this.owner = owner;
        }
        #endregion

        public override string ToString()
        {
            return this.Owner + ": " + this.Text;
        }
    }
}

